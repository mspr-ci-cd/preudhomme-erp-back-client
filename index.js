/** 
 * @file API Back - Client
 * 
 * @version 1.0
 * @author cbarange
 * @copyright 2021
 * 
 */

require('dotenv').config()

const data = require('./data.json')

const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

const openapi = require('openapi-comment-parser')
const swagger_ui = require('swagger-ui-express')

/**
 * Main variable for express service
 * @type {Object}
 * @const
 */
const app = express()

app.enable('trust proxy')
app.use(express.json())
app.use(morgan('common'))
app.use(cors('*'))
const spec = openapi()
app.use('/docs', swagger_ui.serve, swagger_ui.setup(spec))


/**
 * Variable used as response for main root
 * @type String
 * @const
 */
const main_root_text_response = "Welcome"

/**
 * GET /
 * @summary Returns Welcome
 * @response 200 - String
 */
app.get('/', async (req, res) => res.send(main_root_text_response) )


/**
 * get all the clients in the database
 * @method get_client
 * @returns {json} - The list of Clients
 */
const get_client = () => data

/**
 * GET /client
 * @summary Returns a list of clients
 * @description Retrieves all the clients with their account
 * @response 200 - A JSON array of client
 * @responseContent {string[]} 200.application/json
 */
app.get('/client', async (req, res) => res.json(get_client()) )

/**
 * variable used for unit and integration tests
 * @type {Object}
 * @const
 */
const port = process.env.PORT || 5225

/**
 * variable used for unit and integration tests
 * @type {Object}
 * @const
 */
const server = app.listen(port, () => console.log(`Listening at http://localhost:${port}`))

// Export module for test
module.exports = {app:app,server:server,get_client:get_client}