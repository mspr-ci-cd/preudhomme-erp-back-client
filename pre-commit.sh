#!/bin/bash

#TODO run unit test only if js file extension in added file
echo "Running Unit Test..."
yarn unit

RESULT=$?
if [[ "$RESULT" == 0 ]]; then
    echo "✔ Unit Tests Passed"
    echo "📦 Generate Documentation"
    yarn doc
    git add ./docs
else
    echo "❌ Unit Tests Failed"
    PASS=false
fi

exit $RESULT
