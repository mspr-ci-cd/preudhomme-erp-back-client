const {server, app} = require('./index.js')
const supertest = require('supertest')



afterAll(done => {
  server.close(done);
})


test("GET /client", async () => {
  await supertest(app)
  .get("/client")
  .expect(200)
  .then((response) => {
    
    expect(Array.isArray(response.body)).toBeTruthy()
    expect(response.body.length).toBeGreaterThanOrEqual(1)

      // Check the response data
      expect(response.body[0]).toHaveProperty('id')
      expect(response.body[0]).toHaveProperty('first_name')
      expect(response.body[0]).toHaveProperty('last_name')
      expect(response.body[0]).toHaveProperty('phone_number')
      expect(response.body[0]).toHaveProperty('account')
      expect(response.body[0].account[0]).toHaveProperty('balance')
      expect(response.body[0].account[0]).toHaveProperty('type')
      expect(response.body[0].account[0]).toHaveProperty('created_date')
      
    })
})

test("GET /", async () => {
  await supertest(app)
  .get("/")
  .expect(200)
  .then((response) => {
    expect(response.text).toEqual('Welcome')
    })
})



