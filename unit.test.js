const {server, get_client} = require('./index.js')

afterAll(done => server.close(done))

test("get_client()", async () => {
  expect(get_client()[0]).toHaveProperty('id')
  expect(get_client()[0]).toHaveProperty('first_name')
  expect(get_client()[0]).toHaveProperty('last_name')
  expect(get_client()[0]).toHaveProperty('phone_number')
  expect(get_client()[0]).toHaveProperty('account')
  expect(get_client()[0].account[0]).toHaveProperty('balance')
  expect(get_client()[0].account[0]).toHaveProperty('type')
  expect(get_client()[0].account[0]).toHaveProperty('created_date')
})